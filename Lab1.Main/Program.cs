﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;
using Lab1.Implementation;
namespace Lab1.Main
{
    public class Program
    {
        public IList<ISsak> Met1()
        {
            List<ISsak> lista = new List<ISsak> { };
            Pies p1 = new Pies();
            Pies p2 = new Pies();
            Kot k1 = new Kot();
            Kot k2 = new Kot();
            lista.Add(p1);
            lista.Add(p2);
            lista.Add(k1);
            lista.Add(k2);
            return lista;
        }
        public void Met2(List<ISsak> lista)
        {
            foreach(var i in lista)
            {
                Console.WriteLine(i.Wiek());
            }
        }

        static void Main(string[] args)
        {
            Klasa kl = new Klasa();
            kl.Wiek();

            if (kl is IRobot)
            {
                Console.WriteLine((kl as IRobot).Wiek());
            }
            if (kl is IKot)
            {
                Console.WriteLine((kl as IKot).Wiek());
            }



            Console.ReadKey();
        }
    }
}
