﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;

namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(ISsak);

        public static Type ISub1 = typeof(IKot);
        public static Type Impl1 = typeof(Kot);

        public static Type ISub2 = typeof(IPies);
        public static Type Impl2 = typeof(Pies);


        public static string baseMethod = "Wiek";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "Metoda1";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "Metoda2";
        public static object[] sub2MethodParams = new object[] { };

        #endregion

        #region P2

        public static string collectionFactoryMethod = "Met1";
        public static string collectionConsumerMethod = "Met2";

        #endregion

        #region P3

        public static Type IOther = typeof(IRobot);
        public static Type Impl3 = typeof(Klasa);

        public static string otherCommonMethod = "Wiek";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
